KwestorFont = {};

KwestorFont =
	{
		font_clear_tiny						= { label=L"Clear Tiny",					height=14, frameHeight=16 },
		font_clear_small					= { label=L"Clear Small",					height=16, frameHeight=18 },
		font_clear_medium					= { label=L"Clear Medium",					height=20, frameHeight=22 },
		font_clear_large					= { label=L"Clear Large",					height=24, frameHeight=26 },
		font_clear_small_bold				= { label=L"Clear Small Bold",				height=16, frameHeight=18 },
		font_clear_medium_bold				= { label=L"Clear Medium Bold",				height=20, frameHeight=22 },
		font_clear_large_bold				= { label=L"Clear Large Bold",				height=24, frameHeight=26 },

		font_alert_outline_tiny				= { label=L"Alert Outline Tiny",			height=18, frameHeight=20 },
		font_alert_outline_small			= { label=L"Alert Outline Small",			height=24, frameHeight=26 },
		font_alert_outline_medium			= { label=L"Alert Outline Medium",			height=30, frameHeight=32 },
		font_alert_outline_large			= { label=L"Alert Outline Large",			height=36, frameHeight=38 },
		font_alert_outline_huge				= { label=L"Alert Outline Huge",			height=42, frameHeight=44 },
		font_alert_outline_giant			= { label=L"Alert Outline Giant",			height=48, frameHeight=50 },
		font_alert_outline_gigantic			= { label=L"Alert Outline Gigantic",		height=60, frameHeight=62 },

		font_alert_outline_half_tiny		= { label=L"Alert Outline Half-Tiny",		height=9,  frameHeight=11 },
		font_alert_outline_half_small		= { label=L"Alert Outline Half-Small",		height=12, frameHeight=14 },
		font_alert_outline_half_medium		= { label=L"Alert Outline Half-Medium",		height=15, frameHeight=17 },
		font_alert_outline_half_large		= { label=L"Alert Outline Half-Large",		height=18, frameHeight=20 },
		font_alert_outline_half_huge		= { label=L"Alert Outline Half-Huge",		height=21, frameHeight=23 },
		font_alert_outline_half_giant		= { label=L"Alert Outline Half-Giant",		height=24, frameHeight=26 },
		font_alert_outline_half_gigantic	= { label=L"Alert Outline Half-Gigantic",	height=30, frameHeight=32 },

		font_default_medium_heading			= { label=L"WAR Quest Title",				height=23, frameHeight=25 },
		font_chat_text						= { label=L"WAR Quest Condition",			height=18, frameHeight=20 },
	};

KwestorFont.ComboBoxList =
	{
		[1]  = {key="font_clear_tiny",						label=KwestorFont.font_clear_tiny.label						},
		[2]  = {key="font_clear_small",						label=KwestorFont.font_clear_small.label					},
		[3]  = {key="font_clear_medium",					label=KwestorFont.font_clear_medium.label					},
		[4]  = {key="font_clear_large",						label=KwestorFont.font_clear_large.label					},
		[5]  = {key="font_clear_small_bold",				label=KwestorFont.font_clear_small_bold.label				},
		[6]  = {key="font_clear_medium_bold",				label=KwestorFont.font_clear_medium_bold.label				},
		[7]  = {key="font_clear_large_bold",				label=KwestorFont.font_clear_large_bold.label				},

		[8]  = {key="font_alert_outline_tiny",				label=KwestorFont.font_alert_outline_tiny.label				},
		[9]  = {key="font_alert_outline_small",				label=KwestorFont.font_alert_outline_small.label			},
		[10] = {key="font_alert_outline_medium",			label=KwestorFont.font_alert_outline_medium.label			},
		[11] = {key="font_alert_outline_large",				label=KwestorFont.font_alert_outline_large.label			},
		[12] = {key="font_alert_outline_huge	",			label=KwestorFont.font_alert_outline_huge.label				},
		[13] = {key="font_alert_outline_giant",				label=KwestorFont.font_alert_outline_giant.label			},
		[14] = {key="font_alert_outline_gigantic",			label=KwestorFont.font_alert_outline_gigantic.label			},

		[15] = {key="font_alert_outline_half_tiny",			label=KwestorFont.font_alert_outline_half_tiny.label		},
		[16] = {key="font_alert_outline_half_small",		label=KwestorFont.font_alert_outline_half_small.label		},
		[17] = {key="font_alert_outline_half_medium",		label=KwestorFont.font_alert_outline_half_medium.label		},
		[18] = {key="font_alert_outline_half_large",		label=KwestorFont.font_alert_outline_half_large.label		},
		[19] = {key="font_alert_outline_half_huge",			label=KwestorFont.font_alert_outline_half_huge.label		},
		[20] = {key="font_alert_outline_half_giant",		label=KwestorFont.font_alert_outline_half_giant.label		},
		[21] = {key="font_alert_outline_half_gigantic",		label=KwestorFont.font_alert_outline_half_gigantic.label	},

		[22] = {key="font_default_medium_heading",			label=KwestorFont.font_default_medium_heading.label			},
		[23] = {key="font_chat_text",						label=KwestorFont.font_chat_text.label						},
	};

<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
    <UiMod name="Kwestor" version="2.0.8" date="2009/05/19" >
		<VersionSettings gameVersion="1.4.1" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Author name="rmet0815" email="" />
        <Description text="Kwestor is a drop-in quest tracker replacement for Warhammer Online" />
        <Dependencies>        
			<Dependency name="EATemplate_DefaultWindowSkin" />
            <Dependency name="EA_UiDebugTools" />		
            <Dependency name="EASystem_LayoutEditor" />
            <Dependency name="EA_QuestTrackerWindow" />
            <Dependency name="RMetLib" />
        </Dependencies>
        <SavedVariables>
            <SavedVariable name="KwestorSettings" />
            <SavedVariable name="KwestorZoneArea" />
        </SavedVariables>
        <Files>
            <File name="Gui/KwestorGuiTabGeneral.xml" />
            <File name="Gui/KwestorGuiTabDisplay.xml" />
            <File name="Gui/KwestorGuiTabArea.xml" />
            <File name="Gui/KwestorGui.xml" />
            <File name="Kwestor.xml" />
            <File name="Tracker/KwestorTracker.xml" />
            <File name="KwestorZoneArea.lua" />
            <File name="KwestorSettings.lua" />
            <File name="KwestorFont.lua" />
            <File name="KwestorUtility.lua" />
            <File name="Gui/KwestorGuiUtility.lua" />
            <File name="Gui/KwestorGuiTabGeneral.lua" />
            <File name="Gui/KwestorGuiTabDisplay.lua" />
            <File name="Gui/KwestorGuiTabArea.lua" />
            <File name="Gui/KwestorGui.lua" />
            <File name="Kwestor.lua" />
            <File name="Tracker/KwestorTracker.lua" />
        </Files>
        <OnInitialize>
            <CallFunction name="KwestorGuiUtility.Initialize" />
            <CallFunction name="Kwestor.Initialize" />
            <CallFunction name="KwestorGui.Initialize" />
        </OnInitialize>
        <OnUpdate>
            <CallFunction name="Kwestor.OnUpdate" />
        </OnUpdate>
        <OnShutdown>
            <CallFunction name="Kwestor.Shutdown" />
            <CallFunction name="KwestorGui.Shutdown" />
        </OnShutdown>
		<WARInfo>
    		<Categories>
        		<Category name="QUESTS" />
    		</Categories>
    		<Careers>
        		<Career name="BLACKGUARD" />
        		<Career name="WITCH_ELF" />
        		<Career name="DISCIPLE" />
        		<Career name="SORCERER" />
        		<Career name="IRON_BREAKER" />
        		<Career name="SLAYER" />
        		<Career name="RUNE_PRIEST" />
        		<Career name="ENGINEER" />
        		<Career name="BLACK_ORC" />
        		<Career name="CHOPPA" />
        		<Career name="SHAMAN" />
        		<Career name="SQUIG_HERDER" />
        		<Career name="WITCH_HUNTER" />
        		<Career name="KNIGHT" />
        		<Career name="BRIGHT_WIZARD" />
        		<Career name="WARRIOR_PRIEST" />
        		<Career name="CHOSEN" />
        		<Career name= "MARAUDER" />
        		<Career name="ZEALOT" />
        		<Career name="MAGUS" />
        		<Career name="SWORDMASTER" />
        		<Career name="SHADOW_WARRIOR" />
        		<Career name="WHITE_LION" />
        		<Career name="ARCHMAGE" />
    		</Careers>
		</WARInfo>
    </UiMod>
</ModuleFile>    

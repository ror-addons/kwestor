===v2.0.8===

# xml-savesettings fix for WAR 1.3.1 release
# added <warinfo> tags to modfile

===v2.0.6===

# Added Quest Highlight (alpha for quest-text, higher alpha on mouseover)

===v2.0.4===

# Upgraded to game version 1.3.0

===v2.0.2===

# RMetLib is now included in the download again

===v2.0.0===

# changed to svn and using curseforge release structure

===v1.0.12===

# Quest which are not drawn on map (have no trackingPin) now enclosed by <>
# BUGFIX: slash-command now registers correctly and only once

===v1.0.11===

# You can now link quests to the chat window (LEFT-click icon next to quest on tracker)

===v1.0.10===

# WAR 1.2.1 changes

===v1.0.9===

# cleaned up dependencies

===v1.0.8===

# Fixed a bug with max quests in GUI

===v1.0.7===

# Minor GUI layout rework

===v1.0.6===

# Fixed a quirk of WAR 1.2 release with autoresize labels

===v1.0.5===

# BUGFIX: Quest timer no longer chops off last few digits and produces warnings (thanx to Bloodwalker)

===v1.0.4===

# RvR filter now depending on zone/area, and not player rvr status
# Number of quests tracked by the tracker now adjustable
# GUI: Added tracked RvR zones
# BUGFIX: condition of original WAR quest tracker restored on shutdown

===v1.0.3===

# added tracker mode: select vs autofill

===v1.0.2===

# added settings for background (mode, color, alpha)
# added option to grow tracker upwards
# BUGFIX: gui elements now clickable again

===v1.0.1===

# QuestTracker toggle implemented
# added new autohide option: Quest in different zone
# added default WAR quest fonts to font selection
# INTERNAL: optimized zone filters

===v1.0.0===

# Initial Release

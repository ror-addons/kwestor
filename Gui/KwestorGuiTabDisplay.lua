KwestorGuiTabDisplay = {};

KwestorGuiTabDisplay.questTrackerAlignmentList =
	{
		[1] = {key="LEFT",	label=L"Left aligned"},
		[2] = {key="RIGHT", label=L"Right aligned"},
	};

KwestorGuiTabDisplay.questTrackerGrowthList =
	{
		[1] = {key="DOWN",	label=L"Downwards"},
		[2] = {key="UP",	label=L"Upwards"},
	};

KwestorGuiTabDisplay.questCompletedIconList =
	{
		[1] = {key="QUESTTYPE",	label=L"Quest Type Icon"},
		[2] = {key="COMPLETED",	label=L"Completed Icon"},
	};

KwestorGuiTabDisplay.backgroundModeList =
	{
		[1] = {key="TRACKER",	label=L"Tracker"},
		[2] = {key="QUEST",		label=L"Quest"},
		[3] = {key="NONE",		label=L"No background"},
	};

KwestorGuiTabDisplay.alphaList =
	{
		[1]  = {key=10,		label=L"10%"},
		[2]  = {key=20,		label=L"20%"},
		[3]  = {key=30,		label=L"30%"},
		[4]  = {key=40,		label=L"40%"},
		[5]  = {key=50,		label=L"50%"},
		[6]  = {key=60,		label=L"60%"},
		[7]  = {key=70,		label=L"70%"},
		[8]  = {key=80,		label=L"80%"},
		[9]  = {key=90,		label=L"90%"},
		[10] = {key=100,	label=L"100%"},
	};

-- =========================================================================================
-- Initialize
-- =========================================================================================

function KwestorGuiTabDisplay.Initialize()

	local obj;

	-- ----- quest tracker heading ---------------
	LabelSetText( "KwestorGui_KwestorTabDisplayQuestTrackerFrameHeading", L"Quest Tracker" );

	-- ----- quest tracker alignment --------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTrackerAlignment", 
                               settingObj='Kwestor', settingKey='questTrackerAlignment',
                               label=L"Alignment", valueList=KwestorGuiTabDisplay.questTrackerAlignmentList };
	obj:AddCallbackPostChange( KwestorTracker, 'UpdateAlignment' );
	RMetGuiBroker.Add( obj );

	-- ----- quest tracker growth -----------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTrackerGrowth", 
                               settingObj='Kwestor', settingKey='questTrackerGrowth',
                               label=L"Growth", valueList=KwestorGuiTabDisplay.questTrackerGrowthList };
	obj:AddCallbackPostChange( KwestorTracker, 'UpdateAlignment' );
	RMetGuiBroker.Add( obj );

	-- ----- quest compleed icon ------------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestCompletedIcon", 
                               settingObj='Kwestor', settingKey='questCompletedIcon',
                               label=L"Completed Icon", valueList=KwestorGuiTabDisplay.questCompletedIconList };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest tracker background mode --------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTrackerBackgroundMode", 
                               settingObj='Kwestor', settingKey='questTrackerBackgroundMode',
                               label=L"Backgr. Mode", valueList=KwestorGuiTabDisplay.backgroundModeList };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest tracker background color -------
	obj = RMetGuiColorPickerButton:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTrackerBackgroundColor", 
                               settingObj='Kwestor', settingKey='questTrackerBackgroundColor',
                               label=L"Backgr. Color", sign=L"Background", backgroundColor='WHITE' };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest tracker background alpha -------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTrackerBackgroundAlpha", 
                               settingObj='Kwestor', settingKey='questTrackerBackgroundAlpha',
                               label=L"Backgr. Alpha", valueList=KwestorGuiTabDisplay.alphaList };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest tracker max quests -------------
	obj = RMetGuiSliderBar:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTrackerMaxQuests", 
                               settingObj='Kwestor', settingKey='questTrackerMaxQuests',
                               label=L"Max Quests", minLabel=L"1 Q", maxLabel=L"20 Q", midLabel=L"10", midLabelFormat=L"%d Qs",
                               minVal=1, maxVal=20 };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest highlight heading -------------
	LabelSetText( "KwestorGui_KwestorTabDisplayQuestHighlightFrameHeading", L"Quest Highlighting" );

	-- ----- quest highlight normal alpha ---------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestHighlightNormalAlpha", 
                               settingObj='Kwestor', settingKey='questHighlightNormalAlpha',
                               label=L"Standard Alpha", valueList=KwestorGuiTabDisplay.alphaList };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest highlight mouseover alpha ------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestHighlightMouseoverAlpha", 
                               settingObj='Kwestor', settingKey='questHighlightMouseoverAlpha',
                               label=L"Mouseover Alpha", valueList=KwestorGuiTabDisplay.alphaList };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest title heading -----------------
	LabelSetText( "KwestorGui_KwestorTabDisplayQuestTitleFrameHeading", L"Quest Title" );

	-- ----- quest title font --------------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTitleFont", 
                               settingObj='Kwestor', settingKey='questTitleFont',
                               label=L"Font", valueList=KwestorFont.ComboBoxList };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest title color completed ----------
	obj = RMetGuiColorPickerButton:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTitleColorCompleted", 
                               settingObj='Kwestor', settingKey='questTitleColorCompleted',
                               label=L"Same Zone", sign=L"Completed" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest title color tracked ------------
	obj = RMetGuiColorPickerButton:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTitleColorTracked", 
                               settingObj='Kwestor', settingKey='questTitleColorTracked',
                               label=L"Same Zone", sign=L"Tracked" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest title color completed other zone 
	obj = RMetGuiColorPickerButton:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTitleColorCompletedOtherZone", 
                               settingObj='Kwestor', settingKey='questTitleColorCompletedOtherZone',
                               label=L"Other Zone", sign=L"Completed" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest title color tracked other zone -
	obj = RMetGuiColorPickerButton:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestTitleColorTrackedOtherZone", 
                               settingObj='Kwestor', settingKey='questTitleColorTrackedOtherZone',
                               label=L"Other Zone", sign=L"Tracked" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest condition heading -------------
	LabelSetText( "KwestorGui_KwestorTabDisplayQuestConditionFrameHeading", L"Quest Condition" );

	-- ----- quest condition font ----------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestConditionFont", 
                               settingObj='Kwestor', settingKey='questConditionFont',
                               label=L"Font", valueList=KwestorFont.ComboBoxList };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest condition color completed ------
	obj = RMetGuiColorPickerButton:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestConditionColorCompleted", 
                               settingObj='Kwestor', settingKey='questConditionColorCompleted',
                               label=L"Completed", sign=L"Completed" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest condition color tracked --------
	obj = RMetGuiColorPickerButton:New{ paneID='KwestorTabDisplay', frameName="KwestorGui_KwestorTabDisplayQuestConditionColorTracked", 
                               settingObj='Kwestor', settingKey='questConditionColorTracked',
                               label=L"Tracked", sign=L"Tracked" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- now init the Broker -----------------
	RMetGuiBroker.InitializePane( 'KwestorTabDisplay', KwestorGui.WIDGET_WIDTH );

end

-- =========================================================================================
-- Shutdown
-- =========================================================================================

function KwestorGuiTabDisplay.Shutdown()

	RMetGuiBroker.ShutdownPane( 'KwestorTabDisplay' );

end

-- =========================================================================================
-- Activate
-- =========================================================================================

function KwestorGuiTabDisplay.Activate()

	RMetGuiBroker.ActivatePane( 'KwestorTabDisplay' );

end

KwestorGuiTabArea = {};

KwestorGuiTabArea.SORT_COLUMN_ZONE_NAME = 1;
KwestorGuiTabArea.SORT_COLUMN_AREA_NAME = 2;
KwestorGuiTabArea.SORT_COLUMN_RVR_STATUS = 3;
KwestorGuiTabArea.SORT_COLUMN_SEEN = 4;
KwestorGuiTabArea.MAX_SORT_COLUMN = 4;

KwestorGuiTabArea.currentTab = 1;

KwestorGuiTabArea.listData = {};
KwestorGuiTabArea.listOrder = {};
KwestorGuiTabArea.currentDataIndex = 0;
KwestorGuiTabArea.currentZoneAreaIndex = 0;
KwestorGuiTabArea.sortColumn = KwestorGuiTabArea.SORT_COLUMN_ZONE_NAME;

-- =========================================================================================
-- Initialize
-- =========================================================================================

function KwestorGuiTabArea.Initialize()

	local color;

	-- Labels for Action Buttons
	ButtonSetText( "KwestorGui_KwestorTabAreaListAddArea", L"Add Area" );
	ButtonSetText( "KwestorGui_KwestorTabAreaListRemoveArea", L"Remove Area" );
	ButtonSetText( "KwestorGui_KwestorTabAreaListToggleRvR", L"Toggle RvR" );

	-- Labels for Search Buttons
	ButtonSetText( "KwestorGui_KwestorTabAreaSortButtonBarZoneNameButton", L"Zone Name" );
	ButtonSetText( "KwestorGui_KwestorTabAreaSortButtonBarAreaNameButton", L"Area Name" );
	ButtonSetText( "KwestorGui_KwestorTabAreaSortButtonBarRvRStatusButton", L"RvR Status" );
	ButtonSetText( "KwestorGui_KwestorTabAreaSortButtonBarSeenButton", L"Seen" );

	-- colored row backgrouns
	for row=1, KwestorGui_KwestorTabAreaList.numVisibleRows do
		color = KwestorGuiUtility.GetAlternatingRowColor( math.mod(row,2) );
		WindowSetTintColor( "KwestorGui_KwestorTabAreaListRow"..row.."Background", color.r, color.g, color.b );
	end

end

-- =========================================================================================
-- Shutdown
-- =========================================================================================

function KwestorGuiTabArea.Shutdown()

end

-- =========================================================================================
-- Activate
-- =========================================================================================

function KwestorGuiTabArea.Activate()

	KwestorGuiTabArea.ListUnselectRow();
	KwestorGuiTabArea.ActivateList();

end


-- #############################################################################################################################################################################
-- List Handling
-- #############################################################################################################################################################################

-- =========================================================================================
-- Create the list
-- =========================================================================================

function KwestorGuiTabArea.ActivateList()

	local rvrStatus = L"";
	local i = 1;

	KwestorGuiTabArea.listData = {};
	KwestorGuiTabArea.listOrder = {};

	for k,v in pairs(KwestorZoneArea) do
		if ( v.rvr == true ) then
			rvrStatus = L"RvR";
		else
			rvrStatus = L"Normal";
		end

		KwestorGuiTabArea.listData[i] = {};
		KwestorGuiTabArea.listData[i].zoneAreaIdx = k;
		KwestorGuiTabArea.listData[i].zoneName = v.zoneName;
		KwestorGuiTabArea.listData[i].areaName = v.areaName;
		KwestorGuiTabArea.listData[i].rvrStatus = rvrStatus;
		KwestorGuiTabArea.listData[i].seen = v.seen;

		KwestorGuiTabArea.listOrder[i] = i;

		i = i + 1;
	end

	KwestorGuiTabArea.SortList();

end

-- =========================================================================================
-- Call from the list
-- =========================================================================================

function KwestorGuiTabArea.Populate()

	local color;

	if ( KwestorGui_KwestorTabAreaList.PopulatorIndices == nil ) then
		return;
	end

	for row,idx in ipairs(KwestorGui_KwestorTabAreaList.PopulatorIndices) do

		-- highlight selected (and reset others)
		if ( idx == KwestorGuiTabArea.currentDataIndex ) then
			color = RMetColor.DARK_GOLDENROD;
		else
			color = KwestorGuiUtility.GetAlternatingRowColor( math.mod(row,2) );
		end
		WindowSetTintColor( "KwestorGui_KwestorTabAreaListRow"..row..'Background', color.r, color.g, color.b );

	end

end

-- =========================================================================================
-- Sort the list
-- =========================================================================================

function KwestorGuiTabArea.ListCompareDataSingle( idx1, idx2, cols, colIdx )

	local col = cols[colIdx];

	if ( col == nil ) then
		return( false );
	end

	if ( KwestorZoneArea[idx1][col] == KwestorZoneArea[idx2][col] ) then
		return( KwestorGuiTabArea.ListCompareDataSingle(idx1,idx2,cols,colIdx+1) );
	else
		return( KwestorZoneArea[idx1][col] < KwestorZoneArea[idx2][col] );
	end

	return( false );

end

function KwestorGuiTabArea.ListCompareData( idx1, idx2 )

	local zoneAreaIdx1, zoneAreaIdx;

	if ( idx2 == nil ) then
		return( false );
	end

	if ( KwestorGuiTabArea.listData[idx1] ~= nil ) then
		zoneAreaIdx1 = KwestorGuiTabArea.listData[idx1].zoneAreaIdx;
	end

	if ( KwestorGuiTabArea.listData[idx2] ~= nil ) then
		zoneAreaIdx2 = KwestorGuiTabArea.listData[idx2].zoneAreaIdx;
	end

	if ( zoneAreaIdx1 == nil or zoneAreaIdx2 == nil ) then
		return( false );
	end

	-- ----- Sort by ZoneName -----------
	if ( KwestorGuiTabArea.sortColumn == KwestorGuiTabArea.SORT_COLUMN_ZONE_NAME ) then
		return( KwestorGuiTabArea.ListCompareDataSingle(zoneAreaIdx1,zoneAreaIdx2,{'zoneName','areaName'},1) );

	-- ----- Sort by AreaName -----------
	elseif ( KwestorGuiTabArea.sortColumn == KwestorGuiTabArea.SORT_COLUMN_AREA_NAME ) then
		return( KwestorGuiTabArea.ListCompareDataSingle(zoneAreaIdx1,zoneAreaIdx2,{'areaName','zoneName'},1) );

	-- ----- Sort by RvRStatus ----------
	elseif ( KwestorGuiTabArea.sortColumn == KwestorGuiTabArea.SORT_COLUMN_RVR_STATUS ) then
		return( KwestorGuiTabArea.ListCompareDataSingle(zoneAreaIdx1,zoneAreaIdx2,{'rvr','zoneName','areaName'},1) );

	-- ----- Sort by Seen ---------------
	elseif ( KwestorGuiTabArea.sortColumn == KwestorGuiTabArea.SORT_COLUMN_SEEN ) then
		return( KwestorGuiTabArea.ListCompareDataSingle(zoneAreaIdx1,zoneAreaIdx2,{'seen','zoneName','areaName'},1) );

	-- ----- Something else ---------------
	else
		return( false );
	end

end

function KwestorGuiTabArea.SortList()

	table.sort( KwestorGuiTabArea.listOrder, KwestorGuiTabArea.ListCompareData );
	ListBoxSetDisplayOrder( "KwestorGui_KwestorTabAreaList", KwestorGuiTabArea.listOrder );

	-- SocialWindowTabFriends.UpdateSortButtons()					TODO (the arrows)

end

function KwestorGuiTabArea.OnSortList()

	KwestorGuiTabArea.sortColumn = WindowGetId( SystemData.ActiveWindow.name );
	KwestorGuiTabArea.SortList();

end

-- =========================================================================================
-- List, row selected
-- =========================================================================================

function KwestorGuiTabArea.ListSelectRow()

	local rowNum = WindowGetId( SystemData.ActiveWindow.name );
	local dataIndex = KwestorGui_KwestorTabAreaList.PopulatorIndices[rowNum];		-- index of 'original' list

	KwestorGuiTabArea.currentDataIndex = dataIndex;
	KwestorGuiTabArea.currentZoneAreaIndex = KwestorGuiTabArea.listData[dataIndex].zoneAreaIdx;

	ButtonSetDisabledFlag( "KwestorGui_KwestorTabAreaListRemoveArea", false );
	ButtonSetDisabledFlag( "KwestorGui_KwestorTabAreaListToggleRvR", false );

	KwestorGuiTabArea.Populate();

end

-- =========================================================================================
-- List, unselect the row
-- =========================================================================================

function KwestorGuiTabArea.ListUnselectRow()

	KwestorGuiTabArea.currentDataIndex = 0;
	KwestorGuiTabArea.currentZoneAreaIndex = 0;

	ButtonSetDisabledFlag( "KwestorGui_KwestorTabAreaListRemoveArea", true );
	ButtonSetDisabledFlag( "KwestorGui_KwestorTabAreaListToggleRvR", true );

	KwestorGuiTabArea.Populate();

end

-- =========================================================================================
-- List, add an entry (area)
-- =========================================================================================

function KwestorGuiTabArea.ListAddArea()

	Kwestor.AddCurrentZoneArea();

	KwestorGuiTabArea.Activate();
	KwestorTracker.EventQuestListUpdated();

end

-- =========================================================================================
-- List, remove an entry (area)
-- =========================================================================================

function KwestorGuiTabArea.ListRemoveArea()

	if ( KwestorGuiTabArea.currentZoneAreaIndex == 0 ) then
		return;
	end

	Kwestor.RemoveZoneArea( KwestorGuiTabArea.currentZoneAreaIndex );

	KwestorGuiTabArea.Activate();
	KwestorTracker.EventQuestListUpdated();

end

-- =========================================================================================
-- List, toggle RvR status of an Area
-- =========================================================================================

function KwestorGuiTabArea.ListToggleRvR()

	if ( KwestorGuiTabArea.currentZoneAreaIndex == 0 ) then
		return;
	end

	Kwestor.ZoneAreaToggleRvR( KwestorGuiTabArea.currentZoneAreaIndex );

	KwestorGuiTabArea.Activate();
	KwestorTracker.EventQuestListUpdated();

end

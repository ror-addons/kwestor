KwestorGuiTabGeneral = {};

KwestorGuiTabGeneral.questSortCriteriaList = {
	[1] = {key="ZONE",		label=L"zone"},
	[2] = {key="STATUS",	label=L"quest status"},
	[3] = {key="TITLE",		label=L"quest title"},
	[4] = {key="NONE",		label=L"nothing"},
};

-- =========================================================================================
-- Initialize
-- =========================================================================================

function KwestorGuiTabGeneral.Initialize()

	-- ----- quest tracker mode frame ------------
	LabelSetText( "KwestorGui_KwestorTabGeneralQuestTrackerModeFrameHeading", L"Quest Tracker Mode" );

	-- ----- quest tracker mode checked -----------
	obj = RMetGuiRadioButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestTrackerModeChecked", 
                               groupID="trackerMode", valueOn=false,
                               settingObj='Kwestor', settingKey='questTrackerAutofill',
                               label=L"Track only selected quests, apply filter to these" };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest tracker mode autofill ----------
	obj = RMetGuiRadioButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestTrackerModeAutofill", 
                               groupID="trackerMode", valueOn=true,
                               settingObj='Kwestor', settingKey='questTrackerAutofill',
                               label=L"Lock selected quests and autofill according to filter" };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest filter frame ------------------
	LabelSetText( "KwestorGui_KwestorTabGeneralQuestFilterFrameHeading", L"Quest Filters" );

	-- ----- quest filter zone --------------------
	obj = RMetGuiCheckButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestFilterZone", 
                               settingObj='Kwestor', settingKey='questFilterZone',
                               label=L"Zone" };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest filter rvr ---------------------
	obj = RMetGuiCheckButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestFilterRvR", 
                               settingObj='Kwestor', settingKey='questFilterRvR',
                               label=L"RvR" };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest filter status ------------------
	obj = RMetGuiCheckButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestFilterStatus", 
                               settingObj='Kwestor', settingKey='questFilterStatus',
                               label=L"Complete" };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest condition filter frame --------
	LabelSetText( "KwestorGui_KwestorTabGeneralQuestConditionFilterFrameHeading", L"Quest Condition Autohide" );

	-- ----- quest condition filter completed single objective
	obj = RMetGuiCheckButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestConditionFilterCompletedSingleObjective", 
                               settingObj='Kwestor', settingKey='questConditionFilterCompletedSingleObjective',
                               label=L"Completed, single objective" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest condition filter completed with counter
	obj = RMetGuiCheckButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestConditionFilterCompletedWithCounter", 
                               settingObj='Kwestor', settingKey='questConditionFilterCompletedWithCounter',
                               label=L"Completed with counter" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest condition filter other zone ----
	obj = RMetGuiCheckButton:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestConditionFilterOtherZone", 
                               settingObj='Kwestor', settingKey='questConditionFilterOtherZone',
                               label=L"Quest is in different zone" };
	obj:AddCallbackPostChange( KwestorTracker, 'DrawTracker' );
	RMetGuiBroker.Add( obj );

	-- ----- quest sort frame --------------------
	LabelSetText( "KwestorGui_KwestorTabGeneralQuestSortFrameHeading", L"Quest Sorting" );

	-- ----- quest sort criteria 1 ----------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestSortCriteria1", 
                               settingObj='Kwestor', settingKey='questSortCriteria1',
                               label=L"Criteria #1", valueList=KwestorGuiTabGeneral.questSortCriteriaList };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest sort criteria 2 ----------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestSortCriteria2", 
                               settingObj='Kwestor', settingKey='questSortCriteria2',
                               label=L"Criteria #2", valueList=KwestorGuiTabGeneral.questSortCriteriaList };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- quest sort criteria 3 ----------------
	obj = RMetGuiComboBox:New{ paneID='KwestorTabGeneral', frameName="KwestorGui_KwestorTabGeneralQuestSortCriteria3", 
                               settingObj='Kwestor', settingKey='questSortCriteria3',
                               label=L"Criteria #3", valueList=KwestorGuiTabGeneral.questSortCriteriaList };
	obj:AddCallbackPostChange( KwestorTracker, 'EventQuestListUpdated' );
	RMetGuiBroker.Add( obj );

	-- ----- now init the Broker -----------------
	RMetGuiBroker.InitializePane( 'KwestorTabGeneral', KwestorGui.WIDGET_WIDTH );

end

-- =========================================================================================
-- Shutdown
-- =========================================================================================

function KwestorGuiTabGeneral.Shutdown()

	RMetGuiBroker.ShutdownPane( 'KwestorTabGeneral' );

end

-- =========================================================================================
-- Activate
-- =========================================================================================

function KwestorGuiTabGeneral.Activate()

	RMetGuiBroker.ActivatePane( 'KwestorTabGeneral' );

end


KwestorGui = {};

KwestorGui.ROOT_WINDOW_WIDTH = 1000;
KwestorGui.ROOT_WINDOW_HEIGHT = 800;

KwestorGui.WIDGET_WIDTH = 400;

KwestorGui.tabs = { [1] = {buttonName="KwestorGui_KwestorTabSelectGeneral", 
                            buttonLabel=L"General", 
                            frameName="KwestorGui_KwestorTabGeneral", 
                            obj=nil
                           },
                     [2] = {buttonName="KwestorGui_KwestorTabSelectDisplay",            
                            buttonLabel=L"Display",     
                            frameName="KwestorGui_KwestorTabDisplay",     
                            obj=nil
							},
                     [3] = {buttonName="KwestorGui_KwestorTabSelectArea",            
                            buttonLabel=L"RvR Areas",     
                            frameName="KwestorGui_KwestorTabArea",     
                            obj=nil
                           },
				   };
KwestorGui.NUM_TABS = 3;

KwestorGui.currentTab = 1;

-- =========================================================================================
-- Initialize
-- =========================================================================================

function KwestorGui.Initialize()

	CreateWindow( "KwestorGui_Kwestor", false );

	KwestorGui.tabs[1].obj = KwestorGuiTabGeneral;
	KwestorGui.tabs[2].obj = KwestorGuiTabDisplay;
	KwestorGui.tabs[3].obj = KwestorGuiTabArea;

	for i=1,KwestorGui.NUM_TABS do
		ButtonSetText( KwestorGui.tabs[i].buttonName, KwestorGui.tabs[i].buttonLabel );
		ButtonSetStayDownFlag( KwestorGui.tabs[i].buttonName, true );
		WindowSetShowing( KwestorGui.tabs[i].frameName, false );
		KwestorGui.tabs[i].obj.Initialize();
	end

	WindowSetDimensions( 'KwestorGui_Kwestor', KwestorGui.ROOT_WINDOW_WIDTH, KwestorGui.ROOT_WINDOW_HEIGHT );

	LabelSetText( "KwestorGui_KwestorTitleBarText", L"Kwestor "..KwestorUtility.WFormatVersion(Kwestor.VERSION) );

end

-- =========================================================================================
-- Shutdown
-- =========================================================================================

function KwestorGui.Shutdown()

	for i=1,KwestorGui.NUM_TABS do
		KwestorGui.tabs[i].obj.Shutdown();
	end
end

-- =========================================================================================
-- Open Gui
-- =========================================================================================

function KwestorGui.OpenGui()

	WindowSetShowing( "KwestorGui_Kwestor", true );
	KwestorGui.SwitchTab( KwestorGui.currentTab );

end

-- =========================================================================================
-- Close Gui
-- =========================================================================================

function KwestorGui.CloseGui()

	WindowSetShowing( "KwestorGui_Kwestor", false );

end

-- =========================================================================================
-- Toggle Gui
-- =========================================================================================

function KwestorGui.ToggleGui()

	if ( WindowGetShowing("KwestorGui_Kwestor") ) then
		KwestorGui.CloseGui();
	else
		KwestorGui.OpenGui();
	end

end

-- =========================================================================================
-- Switch Tabs
-- =========================================================================================

function KwestorGui.SwitchTab( newTab )

	if ( newTab == nil or newTab == 0 ) then
		newTab = WindowGetId( SystemData.ActiveWindow.name );
	end

	WindowSetShowing( KwestorGui.tabs[KwestorGui.currentTab].frameName, false );
	ButtonSetPressedFlag( KwestorGui.tabs[KwestorGui.currentTab].buttonName, false );
	KwestorGui.currentTab = newTab;
	KwestorGui.tabs[KwestorGui.currentTab].obj.Activate();
	WindowSetShowing( KwestorGui.tabs[KwestorGui.currentTab].frameName, true );
	ButtonSetPressedFlag( KwestorGui.tabs[KwestorGui.currentTab].buttonName, true );

end

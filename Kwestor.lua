Kwestor = {};

Kwestor.VERSION = 02000008;							-- vMM.RRR.mmm

Kwestor.settingsIndex = "";

Kwestor.captureRvRArea = false;

-- =====================================================================================================
-- Initialization
-- =====================================================================================================

function Kwestor.Initialize()

	-- initialize and update the settings
	Kwestor.SettingsInitialize();

	-- the tracker
	KwestorTracker.Initialize();

	-- hide the WAR quest tracker
	Kwestor.HideWARQuestTracker();

	-- loading finished handler
	RegisterEventHandler( SystemData.Events.LOADING_END, "Kwestor.OnLoadingEnd" );
	RegisterEventHandler( SystemData.Events.RELOAD_INTERFACE, "Kwestor.OnLoadingEnd" );

	-- events for RvR capture
	RegisterEventHandler( SystemData.Events.PLAYER_AREA_NAME_CHANGED, "Kwestor.EventAreaChanged" );
	RegisterEventHandler( SystemData.Events.PLAYER_START_RVR_FLAG_TIMER, "Kwestor.EventRvRFlagTimerStarted" );

	-- DEBUG / TEST
--[[
	RegisterEventHandler( SystemData.Events.INTERACT_ACCEPT_QUEST, "Kwestor.InteractAcceptQuest" );
	RegisterEventHandler( SystemData.Events.INTERACT_COMPLETE_QUEST, "Kwestor.InteractCompleteQuest" );
	RegisterEventHandler( SystemData.Events.INTERACT_SELECT_QUEST, "Kwestor.InteractSelectQuest" );
	RegisterEventHandler( SystemData.Events.INTERACT_SHOW_QUEST, "Kwestor.InteractShowQuest" );
	RegisterEventHandler( SystemData.Events.QUEST_CONDITION_UPDATED, "Kwestor.QuestConditionUpdated" );
	RegisterEventHandler( SystemData.Events.PLAYER_ZONE_CHANGED, "Kwestor.PlayerZoneChanged" );
]]
end

--[[
function Kwestor.InteractAcceptQuest() d( "KWESTOR-EVENT: INTERACT_ACCEPT_QUEST" ); end;
function Kwestor.InteractCompleteQuest() d( "KWESTOR-EVENT: INTERACT_COMPLETE_QUEST" ); end;
function Kwestor.InteractSelectQuest() d( "KWESTOR-EVENT: INTERACT_SELECT_QUEST" ); end;
function Kwestor.InteractShowQuest() d( "KWESTOR-EVENT: INTERACT_SHOW_QUEST" ); end;
function Kwestor.QuestConditionUpdated() d( "KWESTOR-EVENT: QUEST_CONDITION_UPDATED" ); end;
function Kwestor.PlayerZoneChanged( a, b, c )
	d( "KWESTOR-EVENT: PLAYER_ZONE_CHANGED" );
	d( a or 'NIL' ); d( b or 'NIL' ); d( c or 'NIL' );
end
]]

-- =====================================================================================================
-- Shutdown
-- =====================================================================================================

function Kwestor.Shutdown()

	Kwestor.ShowWARQuestTracker();

end

-- =====================================================================================================
-- Event loading finsihed
-- =====================================================================================================

function Kwestor.OnLoadingEnd()

	-- register the slash cmd
	Kwestor.RegisterLibSlash();

end

-- =====================================================================================================
-- Update handling
-- =====================================================================================================

function Kwestor.OnUpdate( timePassed )

	-- ----- call the update handlers
	KwestorTracker.OnUpdate( timePassed );

end

-- =====================================================================================================
-- Settings
-- =====================================================================================================

function Kwestor.SettingsInitialize()

	local saveFileVersion = nil;

	Kwestor.settingsIndex = WStringToString( wstring.sub( GameData.Player.name, 1, wstring.len(GameData.Player.name)-2 ) );

	if ( KwestorSettings == nil ) then
		KwestorSettings = {};
	end

	if ( KwestorSettings[Kwestor.settingsIndex] == nil ) then
		KwestorSettings[Kwestor.settingsIndex] = {};
	end

	saveFileVersion = (KwestorSettings[Kwestor.settingsIndex].version or 01000000);

	-- ----- pre-default: update version
	if ( saveFileVersion < Kwestor.VERSION and saveFileVersion > 01000000 ) then
--[[
		if ( saveFileVersion < 01004012 ) then
			KwestorSettings[Kwestor.settingsIndex].mouseClickMap = nil;
		end
]]
	end

	-- ----- init with defaults
	for k,v in pairs(KwestorSettingsDefault) do
		if ( KwestorSettings[Kwestor.settingsIndex][k] == nil ) then
			if ( type(v) == 'table' ) then
				KwestorSettings[Kwestor.settingsIndex][k] = KwestorUtility.CopyTable( v );
			else
				KwestorSettings[Kwestor.settingsIndex][k] = v;
			end
		end
	end

	-- ----- post-default: update version
	if ( saveFileVersion < Kwestor.VERSION and saveFileVersion > 01000000 ) then
	end

	-- ----- cleanup "old", no longer used settings
	for k,_ in pairs(KwestorSettings[Kwestor.settingsIndex]) do
		if ( KwestorSettingsDefault[k] == nil ) then
			KwestorSettings[Kwestor.settingsIndex][k] = nil;
		end
	end

	-- ----- save current version
	KwestorSettings[Kwestor.settingsIndex].version = Kwestor.VERSION;

end

function Kwestor.SettingGet( k )

	return( KwestorSettings[Kwestor.settingsIndex][k] );

end

function Kwestor.SettingSet( k, v )

	if ( KwestorSettingsDefault[k] == nil ) then											-- no such setting!
		d( "Kwestor.SettingSet(): No such setting!" ); d( k ); d( v );
		return;
	elseif ( type(v) == 'table' ) then
		KwestorSettings[Kwestor.settingsIndex][k] = KwestorUtility.CopyTable( v );
	else
		KwestorSettings[Kwestor.settingsIndex][k] = v;
	end

end

-- =====================================================================================================
-- RvR Zone capturing
-- =====================================================================================================

function Kwestor.GetZoneAreaIndex( zone, areaName )

	return( string.format("z%05d%s",zone,WStringToString(KwestorUtility.WStringChop(areaName))) );

end

function Kwestor.IsCurrentZoneAreaRvR()

	local idx = nil;
	local zone = GameData.Player.zone;
	local areaName = GameData.Player.area.name;

	if ( areaName ~= nil and areaName ~= L"" and areaName:sub(1,3):upper() ~= L'RVR' ) then
		idx = Kwestor.GetZoneAreaIndex( zone, areaName );

		if ( KwestorZoneArea[idx] and KwestorZoneArea[idx].rvr == true ) then
			return( true );
		end
	end

	return( false );

end
	
function Kwestor.EventAreaChanged( )

	local idx = nil;
	local zone = GameData.Player.zone;
	local areaName = GameData.Player.area.name;

	if ( Kwestor.captureRvRArea == true and areaName ~= nil and areaName ~= L"" and areaName:sub(1,3):upper() ~= L'RVR' ) then
		Kwestor.captureRvRArea = false;

		idx = Kwestor.GetZoneAreaIndex( zone, areaName );

		if ( not KwestorZoneArea[idx] ) then
			KwestorZoneArea[idx] = {rvr=true,zoneName=GetZoneName(zone),areaName=areaName,seen=1};
		elseif ( KwestorZoneArea[idx].seen < 100 ) then
			KwestorZoneArea[idx].seen = KwestorZoneArea[idx].seen + 1;
		end

		KwestorTracker.EventAreaChanged();
	end

end

function Kwestor.EventRvRFlagTimerStarted()

	Kwestor.captureRvRArea = true;

end

function Kwestor.AddCurrentZoneArea()

	local idx = nil;
	local zone = GameData.Player.zone;
	local areaName = GameData.Player.area.name;

	if ( zone ~= nil and zone > 0 and areaName ~= nil and areaName ~= L"" and areaName:sub(1,3):upper() ~= L'RVR' ) then
		idx = Kwestor.GetZoneAreaIndex( zone, areaName );
		if ( not KwestorZoneArea[idx] ) then
			KwestorZoneArea[idx] = {rvr=true,zoneName=GetZoneName(zone),areaName=areaName,seen=1};
		end
	end

end

function Kwestor.RemoveZoneArea( idx )

	KwestorZoneArea[idx] = nil;

end

function Kwestor.ZoneAreaToggleRvR( idx )

	if ( KwestorZoneArea[idx] ) then
		KwestorZoneArea[idx].rvr = not KwestorZoneArea[idx].rvr;
	end

end

function Kwestor.ZoneAreaAddTestData()

	KwestorZoneArea['00001A'] = {zoneName=L"Zone-A", areaName=L"Area-A", seen=1, rvr=true};
	KwestorZoneArea['00002B'] = {zoneName=L"Zone-B", areaName=L"Area-B", seen=1, rvr=true};
	KwestorZoneArea['00003C'] = {zoneName=L"Zone-C", areaName=L"Area-C", seen=1, rvr=true};
	KwestorZoneArea['00004D'] = {zoneName=L"Zone-D", areaName=L"Area-D", seen=1, rvr=true};
	KwestorZoneArea['00005E'] = {zoneName=L"Zone-E", areaName=L"Area-E", seen=1, rvr=true};
	KwestorZoneArea['00006F'] = {zoneName=L"Zone-F", areaName=L"Area-F", seen=1, rvr=true};
	KwestorZoneArea['00007G'] = {zoneName=L"Zone-G", areaName=L"Area-G", seen=1, rvr=true};
	KwestorZoneArea['00008H'] = {zoneName=L"Zone-H", areaName=L"Area-H", seen=1, rvr=true};
	KwestorZoneArea['00009I'] = {zoneName=L"Zone-I", areaName=L"Area-I", seen=1, rvr=true};
	KwestorZoneArea['00010J'] = {zoneName=L"Zone-J", areaName=L"Area-J", seen=1, rvr=true};
	KwestorZoneArea['00011K'] = {zoneName=L"Zone-K", areaName=L"Area-K", seen=1, rvr=true};
	KwestorZoneArea['00012L'] = {zoneName=L"Zone-L", areaName=L"Area-L", seen=1, rvr=true};
	KwestorZoneArea['00013M'] = {zoneName=L"Zone-M", areaName=L"Area-M", seen=1, rvr=true};
	KwestorZoneArea['00014N'] = {zoneName=L"Zone-N", areaName=L"Area-N", seen=1, rvr=true};
	KwestorZoneArea['00015O'] = {zoneName=L"Zone-O", areaName=L"Area-O", seen=1, rvr=true};
	KwestorZoneArea['00016P'] = {zoneName=L"Zone-P", areaName=L"Area-P", seen=1, rvr=true};
	KwestorZoneArea['00017Q'] = {zoneName=L"Zone-Q", areaName=L"Area-Q", seen=1, rvr=true};
	KwestorZoneArea['00018R'] = {zoneName=L"Zone-R", areaName=L"Area-R", seen=1, rvr=true};
	KwestorZoneArea['00019S'] = {zoneName=L"Zone-S", areaName=L"Area-S", seen=1, rvr=true};
	KwestorZoneArea['00020T'] = {zoneName=L"Zone-T", areaName=L"Area-T", seen=1, rvr=true};

end

-- =====================================================================================================
-- Show / Hide the WAR tracker
-- =====================================================================================================

function Kwestor.ShowWARQuestTracker()

	WindowSetShowing( 'EA_Window_QuestTrackerNub', true );

end

function Kwestor.HideWARQuestTracker()

	if ( WindowGetShowing("EA_Window_QuestTracker") == true ) then
		EA_Window_QuestTracker.ToggleShowing();
	end

	WindowSetShowing( 'EA_Window_QuestTrackerNub', false );

end

-- =====================================================================================================
-- Slash commands
-- =====================================================================================================

function Kwestor.RegisterLibSlash()

	if ( LibSlash and (not LibSlash.IsSlashCmdRegistered('kwestor')) ) then
		LibSlash.RegisterSlashCmd("kwestor",
			function(args)
				KwestorGui.OpenGui();
			end
		)
	end

end

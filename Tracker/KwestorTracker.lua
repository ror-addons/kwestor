KwestorTracker = {};

KwestorTracker.QUEST_ICON_SIZE = 32;
KwestorTracker.CLOCK_ICON_SIZE = 20;
KwestorTracker.ICON_NAME_GAP = 5;
KwestorTracker.TIMER_GAP = 5;
KwestorTracker.TIMED_QUESTS_UPDATE_THROTTLE = 0.2;
KwestorTracker.QUEST_BACKGROUND_BORDER = 3;
KwestorTracker.TRACKER_BACKGROUND_BORDER = 5;

KwestorTracker.trackerIsAnchored = false;					-- at startup, tracker needs to be anchored

KwestorTracker.updateDelay = 0;

KwestorTracker.trackedQuests = {};							-- { questID = {questData}, ...}
KwestorTracker.orderedQuests = {};							-- { idx = questID, ... }
KwestorTracker.timedQuests = {};							-- { questID = questID, ... }
KwestorTracker.tmpQuests = {};								-- temporary quest cache
KwestorTracker.knownQuests = {};							-- list with currently known quest { questID = questID, .. }

KwestorTracker.anchorWidth = 300;
KwestorTracker.anchorHeight = 400;

KwestorTracker.playerInRvRZoneArea = false;					-- is player in RvR zone/area

KwestorTracker.mouseOverQuestID = -1;						-- ID of the quest mouse is currently hovering over (or -1)

-- =====================================================================================================
-- Initialization
-- =====================================================================================================

function KwestorTracker.Initialize()

--	KwestorTracker.DebugTimerInit();

	-- ----- the Quest Tracker Nub
	CreateWindow( "UI_KwestorTrackerNub", true );
	ButtonSetStayDownFlag( "UI_KwestorTrackerNubButtonOpen", true );
	ButtonSetStayDownFlag( "UI_KwestorTrackerNubButtonClosed", true );

	LayoutEditor.RegisterWindow( "UI_KwestorTrackerNub", L"Kwestor Tracker Nub", L"Kwestor Tracker Nub",
                                 false, false, false,											-- allowSizeWidth, allowSizeHeight, allowHiding
                                 nil, nil );													-- setHiddenCallback, allowableAnchorList

	-- ----- the Quest Tracker
	CreateWindow( "UI_KwestorTracker", true );
	WindowSetDimensions( "UI_KwestorTracker", KwestorTracker.anchorWidth, KwestorTracker.anchorHeight );

	LayoutEditor.RegisterWindow( "UI_KwestorTracker", L"Kwestor Tracker", L"Kwestor Tracker",
                                 false, false, false,											-- allowSizeWidth, allowSizeHeight, allowHiding
                                 nil, {'topleft','topright','bottomleft','bottomright'} );		-- setHiddenCallback, allowableAnchorList

	-- ----- register layout-editor global callback
	LayoutEditor.RegisterEditCallback( KwestorTracker.DrawTracker );

	-- ----- Register for events
	RegisterEventHandler( SystemData.Events.QUEST_LIST_UPDATED, "KwestorTracker.EventQuestListUpdated" );
	RegisterEventHandler( SystemData.Events.QUEST_INFO_UPDATED, "KwestorTracker.EventQuestInfoUpdated" );
	RegisterEventHandler( SystemData.Events.PLAYER_ZONE_CHANGED, "KwestorTracker.EventQuestListUpdated" );
	RegisterEventHandler( SystemData.Events.PLAYER_AREA_NAME_CHANGED, "KwestorTracker.EventAreaChanged" );
	RegisterEventHandler( SystemData.Events.SCENARIO_BEGIN, "KwestorTracker.EventAreaChanged" );
	RegisterEventHandler( SystemData.Events.SCENARIO_END, "KwestorTracker.EventAreaChanged" );

	-- ----- initial filling
	KwestorTracker.EventQuestListUpdated();

	-- ----- init the nub
	KwestorTracker.RedrawNub();

end

-- =====================================================================================================
-- Shutdown
-- =====================================================================================================

function KwestorTracker.Shutdown()

	KwestorTracker.WipeTracker();

end

-- =====================================================================================================
-- Update handling
-- =====================================================================================================

function KwestorTracker.OnUpdate( timePassed )

	-- --- do i have any timed quests?
	if ( next(KwestorTracker.timedQuests) == nil ) then
		return;
	end

	-- --- check the delay and throttle if required
	KwestorTracker.updateDelay = KwestorTracker.updateDelay - timePassed;
    if ( KwestorTracker.updateDelay > 0 ) then
        return;
    end

	KwestorTracker.updateDelay = KwestorTracker.TIMED_QUESTS_UPDATE_THROTTLE;

	-- --- update the timed quests
	for questID,timerData in pairs(KwestorTracker.timedQuests) do
		KwestorTracker.tmpQuests = DataUtils.GetQuestData( questID );

--		KwestorTracker.tmpQuests.maxTimer = KwestorTracker.DebugTimerMax();
--		KwestorTracker.tmpQuests.timeLeft = KwestorTracker.DebugTimerCurr();

		KwestorTracker.trackedQuests[questID].maxTimer = KwestorTracker.tmpQuests.maxTimer;
		KwestorTracker.trackedQuests[questID].timeLeft = KwestorTracker.tmpQuests.timeLeft;

		if ( KwestorTracker.trackedQuests[questID].maxTimer ~= 0 ) then
			KwestorTracker.DrawQuestTimer( questID );
		else
			KwestorTracker.timedQuests[questID] = nil; 
			KwestorTracker.DrawQuest( questID );
		end

	end

end

-- =====================================================================================================
-- RedrawNub
-- =====================================================================================================

function KwestorTracker.RedrawNub()

	local isOpen = WindowGetShowing( 'UI_KwestorTracker' );

	ButtonSetPressedFlag( "UI_KwestorTrackerNubButtonOpen", isOpen );
	ButtonSetPressedFlag( "UI_KwestorTrackerNubButtonClosed", isOpen );

end

-- =====================================================================================================
-- Nub Mouse Events
-- =====================================================================================================

function KwestorTracker.NubLButtonUp( flags )

	local isOpen = WindowGetShowing( 'UI_KwestorTracker' );

	if ( KwestorUtility.IsShiftPressed(flags) ) then
		EA_Window_QuestTracker.ToggleShowing();
	else
		KwestorTracker.ToggleShowing();
	end

end

function KwestorTracker.NubRButtonUp( flags )
	
	KwestorGui.ToggleGui();

end

-- =====================================================================================================
-- Visibility
-- =====================================================================================================

function KwestorTracker.ShowTracker()

	WindowSetShowing( "UI_KwestorTracker", true );
	Kwestor.SettingSet( "showTracker", true );

	KwestorTracker.EventQuestListUpdated();

	KwestorTracker.RedrawNub();

end

function KwestorTracker.HideTracker()

	WindowSetShowing( "UI_KwestorTracker", false );
	Kwestor.SettingSet( "showTracker", false );

	KwestorTracker.WipeTracker();

	KwestorTracker.RedrawNub();

end

function KwestorTracker.ToggleShowing()

	if ( WindowGetShowing("UI_KwestorTracker") ) then
		KwestorTracker.HideTracker();
	else
		KwestorTracker.ShowTracker();
	end

end

-- =====================================================================================================
-- Event Quest List Updated
-- =====================================================================================================

function KwestorTracker.EventQuestListUpdated()

	local questID;
	local isQuestComplete = false;
	local isQuestInPlayerZone = false;
	local trackQuest = false;
	local autofill = Kwestor.SettingGet( 'questTrackerAutofill' );
	local questFilterZone = Kwestor.SettingGet( 'questFilterZone' );
	local questFilterRvR = Kwestor.SettingGet( 'questFilterRvR' );
	local questFilterStatus = Kwestor.SettingGet( 'questFilterStatus' );
	local orderedQuestIdx = 1;

	if ( Kwestor.SettingGet('showTracker') == false ) then								-- the tracker is hidden
		return;
	end

	KwestorTracker.updateDelay = KwestorTracker.TIMED_QUESTS_UPDATE_THROTTLE;			-- reset the update delay

	KwestorTracker.trackedQuests = {};													-- TODO: do not ALWAYS wipe it
	KwestorTracker.orderedQuests = {};													-- TODO: do not ALWAYS wipe it
	KwestorTracker.timedQuests = {};													-- TODO: do not ALWAYS wipe it
	KwestorTracker.knownQuests = {};
	KwestorTracker.tmpQuests = DataUtils.GetQuests();

	-- ----- is player in an rvr zone/rea (DO AT TOP OF FUNC) -----------
	KwestorTracker.playerInRvRZoneArea = ( GameData.Player.isInScenario or Kwestor.IsCurrentZoneAreaRvR() );

	-- ----- find all quests which are candidates for tracking ----------
	for idx,questData in ipairs(KwestorTracker.tmpQuests) do

--		questData.maxTimer = KwestorTracker.DebugTimerMax();
--		questData.timeLeft = KwestorTracker.DebugTimerCurr();

		KwestorTracker.knownQuests[questData.id] = questData.id;

		isQuestComplete = QuestUtils.IsQuestComplete( questData );
		isQuestInPlayerZone = KwestorTracker.IsQuestInPlayerZone( questData.zones );

		-- ----- quest filtered out ? ----------------------------------
		trackQuest = true;
		if ( questData.name == L"" ) then
			trackQuest = false;
		elseif ( questData.tracking == false and (not autofill) ) then
			trackQuest = false;
		elseif ( questData.tracking == true and autofill ) then
			trackQuest = true;
		else
			if ( questFilterZone and (not isQuestInPlayerZone) ) then
				trackQuest = false;
			elseif ( questFilterRvR and (not KwestorTracker.QuestFilterRvR(questData.questTypes)) ) then
				trackQuest = false;
			elseif ( questFilterStatus and isQuestComplete ) then
				trackQuest = false;
			end
		end

		-- ----- add to tracked quests ---------------------------------
		if ( trackQuest ) then
			-- insert into ordered quests
			KwestorTracker.orderedQuests[orderedQuestIdx] = questData.id;

			-- insert into timedQuests if it has a timer
			if ( questData.maxTimer ~= 0 and (not isQuestComplete) ) then
				KwestorTracker.timedQuests[questData.id] = questData.id;
			end

			-- insert full quest data into tracked quest
			KwestorTracker.trackedQuests[questData.id] = {};
			KwestorUtility.CloneTable( questData, KwestorTracker.trackedQuests[questData.id] );

			-- store the kwestor values
			KwestorTracker.trackedQuests[questData.id].kwestorTrackerSlot = 1;									-- adjusted in (right after) sort()
			KwestorTracker.trackedQuests[questData.id].kwestorIsQuestComplete = isQuestComplete;
			KwestorTracker.trackedQuests[questData.id].kwestorIsQuestInPlayerZone = isQuestInPlayerZone;
			KwestorTracker.trackedQuests[questData.id].kwestorWARIndex = idx;

			-- increase the index
			orderedQuestIdx = orderedQuestIdx + 1;
		end
	end

	-- ----- sort the list
	KwestorTracker.SortQuests();

	-- ----- truncate list
	for idx=#KwestorTracker.orderedQuests,Kwestor.SettingGet('questTrackerMaxQuests')+1,-1 do
		questID = KwestorTracker.orderedQuests[idx];
		KwestorTracker.trackedQuests[questID] = nil;
		KwestorTracker.timedQuests[questID] = nil;
		KwestorTracker.orderedQuests[idx] = nil;
	end

	-- ----- draw the tracker
	KwestorTracker.DrawTracker( false );

end

-- =====================================================================================================
-- Event Quest Info Updated
-- =====================================================================================================

function KwestorTracker.EventQuestInfoUpdated( questID )

	-- ----- disable locked flag if autofill and new quests
	if ( Kwestor.SettingGet('questTrackerAutofill') ) then
		if ( not KwestorTracker.knownQuests[questID] ) then
			SetTrackQuest( questID, false );
		end
	end

end

-- =====================================================================================================
-- Event Area changed 
-- -> if RvR status changed, then update tracker
-- =====================================================================================================

function KwestorTracker.EventAreaChanged()

	local areaIsRvR = Kwestor.IsCurrentZoneAreaRvR();

	if ( KwestorTracker.playerInRvRZoneArea ~= areaIsRvR ) then
		KwestorTracker.EventQuestListUpdated();
	end

end

-- =====================================================================================================
-- Draw the time information of a quest ONLY
-- =====================================================================================================

function KwestorTracker.DrawQuestTimer( questID )

	local timeString = TimeUtils.FormatClock( KwestorTracker.trackedQuests[questID].timeLeft );
	local labelName = "UI_KwestorTrackerContentQuest"..(KwestorTracker.trackedQuests[questID].kwestorTrackerSlot).."TimerValue";
	local w, h = WindowGetDimensions( labelName );

	w = math.max(w,16) * 6;						-- enlarge width to avoid truncate! ("22:34" font-height*6 should be enough)
	WindowSetDimensions( labelName, w, h ); 
	LabelSetText( labelName, timeString );

end

-- =====================================================================================================
-- Draw a single quest
-- =====================================================================================================

function KwestorTracker.DrawQuest( questID, reAnchorAll )

	local x1, y1, x2, y2, frameHeight, frameWidth;
	local fontHeight = 20;
	local questName = L"";
	local questData = KwestorTracker.trackedQuests[questID];
	local frameName = "UI_KwestorTrackerContentQuest"..questData.kwestorTrackerSlot;
	local frameNameBackground = frameName.."Background";
	local frameNameButton = frameName..'Button';
	local frameNameName = frameName..'Name';
	local frameNameTimerClock = frameName..'TimerClock';
	local frameNameTimerValue = frameName..'TimerValue';
	local frameNameCondition = nil;
	local isQuestCompleted = KwestorTracker.IsQuestComplete( questID );
	local isQuestInPlayerZone = KwestorTracker.IsQuestInPlayerZone( questID );
	local numConditions = 0;
	local isConditionComplete = false;
	local trackCondition = true;
	local questConditionFilterCompletedSingleObjective = Kwestor.SettingGet( 'questConditionFilterCompletedSingleObjective' );
	local questConditionFilterCompletedWithCounter = Kwestor.SettingGet( 'questConditionFilterCompletedWithCounter' );
	local questConditionFilterOtherZone = Kwestor.SettingGet( 'questConditionFilterOtherZone' );
	local questTitleFont = Kwestor.SettingGet( 'questTitleFont' );
	local questConditionFont = Kwestor.SettingGet( 'questConditionFont' );
	local questTitleColorCompleted = Kwestor.SettingGet( 'questTitleColorCompleted' );
	local questTitleColorCompletedOtherZone = Kwestor.SettingGet( 'questTitleColorCompletedOtherZone' );
	local questTitleColorTracked = Kwestor.SettingGet( 'questTitleColorTracked' );
	local questTitleColorTrackedOtherZone = Kwestor.SettingGet( 'questTitleColorTrackedOtherZone' );
	local questConditionColorCompleted = Kwestor.SettingGet( 'questConditionColorCompleted' );
	local questConditionColorTracked = Kwestor.SettingGet( 'questConditionColorTracked' );
	local titleFontHeight = math.max( KwestorTracker.QUEST_ICON_SIZE, KwestorTracker.CLOCK_ICON_SIZE, KwestorFont[questTitleFont].frameHeight );
	local conditionFontHeight = KwestorFont[questConditionFont].frameHeight;
	local maxWidth = 0;
	local reAnchorObject = false;
	local fontAlpha = 100;

--	questData.maxTimer = KwestorTracker.DebugTimerMax();
--	questData.timeLeft = KwestorTracker.DebugTimerCurr();

	-- ---------- set "defaults" ------------------------------------------------------------------------------------------------------------------
	if ( KwestorTracker.mouseOverQuestID == questID ) then
		fontAlpha = Kwestor.SettingGet( 'questHighlightMouseoverAlpha' );
	else
		fontAlpha = Kwestor.SettingGet( 'questHighlightNormalAlpha' );
	end

	-- ---------- Enclosing frame -----------------------------------------------------------------------------------------------------------------
	if ( not DoesWindowExist(frameName) ) then
		CreateWindowFromTemplate( frameName, "UI_KwestorTrackerQuestTemplate", "UI_KwestorTrackerContent" );
		reAnchorAll = true;																							-- this is a NEW frame, reanchor ALL data
	end

	if ( reAnchorAll ) then
		if ( questData.kwestorTrackerSlot == 1 ) then
			KwestorTracker.SetAnchor( frameName, 'TOPLEFT', "UI_KwestorTrackerContent", 'TOPLEFT', 0, 0 );
		else
			KwestorTracker.SetAnchor( frameName, 'BOTTOMLEFT', "UI_KwestorTrackerContentQuest"..(questData.kwestorTrackerSlot-1), 'TOPLEFT', 0, 0 );
		end

		WindowClearAnchors( frameNameBackground );
		if ( Kwestor.SettingGet('questTrackerAlignment') == 'LEFT' ) then
			x1,y1 = KwestorTracker.QUEST_ICON_SIZE+KwestorTracker.ICON_NAME_GAP-KwestorTracker.QUEST_BACKGROUND_BORDER, 0;
			x2,y2 = KwestorTracker.QUEST_BACKGROUND_BORDER, 0;
		else
			x1,y1 = KwestorTracker.QUEST_BACKGROUND_BORDER*-1, 0;
			x2,y2 = (KwestorTracker.QUEST_ICON_SIZE+KwestorTracker.ICON_NAME_GAP)*-1+KwestorTracker.QUEST_BACKGROUND_BORDER, 0;
		end
		WindowAddAnchor( frameNameBackground, 'topleft', frameName, 'topleft', x1, y1 );
		WindowAddAnchor( frameNameBackground, 'bottomright', frameName, 'bottomright', x2, y2 );
	end

	-- ---------- Quest Name ----------------------------------------------------------------------------------------------------------------------
	if ( reAnchorAll ) then
		KwestorTracker.SetAnchor( frameNameName, 'TOPLEFT', frameName, 'TOPLEFT', KwestorTracker.QUEST_ICON_SIZE+KwestorTracker.ICON_NAME_GAP, 0 );
	end

	LabelSetFont( frameNameName, questTitleFont, 0 );
    WindowSetFontAlpha( frameNameName, fontAlpha / 100 );
	WindowSetDimensions( frameNameName, 1, titleFontHeight );
	questName = questData.name;
	if ( not questData.trackingPin ) then
		questName = L'<'..questName..L'>';
	end
	if ( Kwestor.SettingGet('questTrackerAutofill') and questData.tracking ) then
		questName = L'['..questName..L']';
	end
	LabelSetText( frameNameName, questName );
	if ( isQuestCompleted and isQuestInPlayerZone ) then			-- complete and same zone
		KwestorUtility.LabelSetTextColor( frameNameName, questTitleColorCompleted );
	elseif ( isQuestCompleted ) then								-- complete and other zone
		KwestorUtility.LabelSetTextColor( frameNameName, questTitleColorCompletedOtherZone );
	elseif ( isQuestInPlayerZone ) then								-- pending and same zone
		KwestorUtility.LabelSetTextColor( frameNameName, questTitleColorTracked );
	else															-- pending and other zone
		KwestorUtility.LabelSetTextColor( frameNameName, questTitleColorTrackedOtherZone );
	end
	maxWidth = WindowGetDimensions( frameNameName );

	-- ---------- Quest Button --------------------------------------------------------------------------------------------------------------------
	if ( not DoesWindowExist(frameNameButton) ) then
		CreateWindowFromTemplate( frameNameButton, "UI_KwestorTrackerQuestButtonTemplate", "Root" );
		reAnchorObject = true;
	end

	if ( reAnchorAll or reAnchorObject ) then
		KwestorTracker.SetAnchor( frameNameButton, 'TOPLEFT', frameName, 'TOPLEFT', 0, 0 );

		reAnchorObject = false;
	end

	if ( isQuestCompleted and Kwestor.SettingGet('questCompletedIcon') == 'COMPLETED' ) then
		DynamicImageSetTextureSlice( frameNameButton..'Icon', "QuestCompleted-Gold" );
	else
		DynamicImageSetTextureSlice( frameNameButton..'Icon', QuestUtils.GetSliceForType(questData.questTypes) );
	end
	WindowSetId( frameNameButton, questID );
	WindowSetShowing( frameNameButton, true );

	-- ---------- Timer ---------------------------------------------------------------------------------------------------------------------------
	if ( questData.maxTimer == 0 or isQuestCompleted ) then
		if ( DoesWindowExist(frameNameTimerClock) ) then
			WindowSetShowing( frameNameTimerClock, false );
			WindowDestroy( frameNameTimerClock );
		end
		if ( DoesWindowExist(frameNameTimerValue) ) then
			WindowSetShowing( frameNameTimerValue, false );
			WindowDestroy( frameNameTimerValue );
		end
	else
		if ( not DoesWindowExist(frameNameTimerClock) ) then
			CreateWindowFromTemplate( frameNameTimerClock, "UI_KwestorTrackerQuestTimerClockTemplate", frameName );
			reAnchorObject = true;
		end

		if ( reAnchorAll or reAnchorObject ) then
			KwestorTracker.SetAnchor( frameNameTimerClock, 'RIGHT', frameName.."Name", 'LEFT', KwestorTracker.TIMER_GAP, 0 );
			reAnchorObject = false;
		end

		if ( not DoesWindowExist(frameNameTimerValue) ) then
			CreateWindowFromTemplate( frameNameTimerValue, "UI_KwestorTrackerQuestTimerValueTemplate", frameName );
			reAnchorObject = true;
		end

		if ( reAnchorAll or reAnchorObject ) then
			KwestorTracker.SetAnchor( frameNameTimerValue, 'RIGHT', frameNameTimerClock, 'LEFT', KwestorTracker.TIMER_GAP, 0 );
			reAnchorObject = false;
		end

		LabelSetFont( frameNameTimerValue, questTitleFont, 0 );
		WindowSetDimensions( frameNameTimerValue, 1, titleFontHeight );
		LabelSetText( frameNameTimerValue, TimeUtils.FormatClock(questData.timeLeft) );
		WindowSetFontAlpha( frameNameTimerValue, fontAlpha / 100 );

		WindowSetShowing( frameNameTimerClock, true );
		WindowSetShowing( frameNameTimerValue, true );

		maxWidth = maxWidth + WindowGetDimensions(frameNameTimerClock) + WindowGetDimensions(frameNameTimerValue) + KwestorTracker.TIMER_GAP*2;
	end
	
	-- ---------- Conditions ----------------------------------------------------------------------------------------------------------------------
	for cIdx,cData in ipairs(questData.conditions) do
		isConditionComplete = (cData.curCounter == cData.maxCounter);

		trackCondition = true;
		if ( questConditionFilterCompletedSingleObjective and cData.maxCounter == 0 and isConditionComplete ) then
			trackCondition = false;
		elseif ( questConditionFilterCompletedWithCounter and cData.maxCounter > 0 and isConditionComplete ) then
			trackCondition = false;
		elseif ( questConditionFilterOtherZone and (not isQuestInPlayerZone) ) then
			trackCondition = false;
		end

		if ( trackCondition ) then
			numConditions = numConditions + 1;
			frameNameCondition = frameName.."Condition"..numConditions;
		
			if ( not DoesWindowExist(frameNameCondition) ) then
				CreateWindowFromTemplate( frameNameCondition, "UI_KwestorTrackerQuestConditionTemplate", frameName );
				reAnchorObject = true;
			end

			if ( reAnchorAll or reAnchorObject ) then
				KwestorTracker.SetAnchor( frameNameCondition, 'TOPLEFT', frameName, 'TOPLEFT',
                                      	KwestorTracker.QUEST_ICON_SIZE+KwestorTracker.ICON_NAME_GAP, titleFontHeight + conditionFontHeight*(numConditions-1) );
				reAnchorObject = false;
			end

			LabelSetFont( frameNameCondition, questConditionFont, 0 );
			WindowSetFontAlpha( frameNameCondition, fontAlpha / 100 );
			WindowSetDimensions( frameNameCondition, 1, conditionFontHeight );
			if ( cData.maxCounter > 0 ) then
				LabelSetText( frameNameCondition, L""..cData.name..L" - "..cData.curCounter..L"/"..cData.maxCounter );
			else
				LabelSetText( frameNameCondition, cData.name );
			end
			if ( isConditionComplete ) then
				LabelSetTextColor( frameNameCondition, RMetColor[questConditionColorCompleted].r, RMetColor[questConditionColorCompleted].g, RMetColor[questConditionColorCompleted].b );
			else
				LabelSetTextColor( frameNameCondition, RMetColor[questConditionColorTracked].r, RMetColor[questConditionColorTracked].g, RMetColor[questConditionColorTracked].b );
			end

			maxWidth = math.max( maxWidth, WindowGetDimensions(frameNameCondition) );
		end
	end

	-- ----- enclosing frame	
	frameWidth = maxWidth +  KwestorTracker.QUEST_ICON_SIZE + KwestorTracker.ICON_NAME_GAP;
	frameHeight = titleFontHeight + numConditions*conditionFontHeight;
	WindowSetDimensions( frameName, frameWidth, frameHeight );
	WindowSetShowing( frameName, true );

	-- ----- background
	if ( Kwestor.SettingGet('questTrackerBackgroundMode') == 'QUEST' ) then
		KwestorUtility.WindowSetTintColor( frameNameBackground, Kwestor.SettingGet('questTrackerBackgroundColor') );
		WindowSetAlpha( frameNameBackground, Kwestor.SettingGet('questTrackerBackgroundAlpha')/100 );
		WindowSetShowing( frameNameBackground, true );
	else
		WindowSetShowing( frameNameBackground, false );
	end

	-- ----- fix the scaling
	KwestorTracker.WindowAdjustScale( frameName );
	KwestorTracker.WindowAdjustScale( frameNameButton );

	-- ----- clean up "left over" condition frames
	numConditions = numConditions + 1;
	frameNameCondition = frameName.."Condition"..numConditions;
	while ( DoesWindowExist(frameNameCondition) ) do
		WindowSetShowing( frameNameCondition, false );
		DestroyWindow( frameNameCondition );
		numConditions = numConditions + 1;
		frameNameCondition = frameName.."Condition"..numConditions;
	end

	return frameWidth, frameHeight;

end

-- =====================================================================================================
-- Draw the tracker
-- =====================================================================================================

function KwestorTracker.DrawTracker( reAnchor )

	local frameName, width, height, point;
	local contentFrameName = 'UI_KwestorTrackerContent';
	local backgroundFrameName = 'UI_KwestorTrackerContentBackground';
	local slot = 0;
	local maxWidth = 10;
	local totalHeight = 0;

	if ( Kwestor.SettingGet('showTracker') == false ) then								-- the tracker is hidden
		return;
	end

	if ( KwestorTracker.trackerIsAnchored == false ) then
		KwestorTracker.trackerIsAnchored = true;
		reAnchor = true;
	else
		reAnchor = reAnchor or false;
	end

	-- --- anchor the tracker content
	if ( reAnchor ) then
		WindowClearAnchors( contentFrameName );
		WindowClearAnchors( backgroundFrameName );
		if ( Kwestor.SettingGet('questTrackerAlignment') == 'LEFT' and Kwestor.SettingGet('questTrackerGrowth') == 'DOWN' ) then
			point = 'topleft';
		elseif ( Kwestor.SettingGet('questTrackerAlignment') == 'RIGHT' and Kwestor.SettingGet('questTrackerGrowth') == 'DOWN' ) then
			point = 'topright';
		elseif ( Kwestor.SettingGet('questTrackerAlignment') == 'LEFT' and Kwestor.SettingGet('questTrackerGrowth') == 'UP' ) then
			point = 'bottomleft';
		elseif ( Kwestor.SettingGet('questTrackerAlignment') == 'RIGHT' and Kwestor.SettingGet('questTrackerGrowth') == 'UP' ) then
			point = 'bottomright';
		end
		WindowAddAnchor( contentFrameName, point, 'UI_KwestorTracker', point, 0, 0 );
		WindowAddAnchor( backgroundFrameName, 'topleft', contentFrameName, 'topleft', 
                         KwestorTracker.TRACKER_BACKGROUND_BORDER*-1, KwestorTracker.TRACKER_BACKGROUND_BORDER*-1 );
		WindowAddAnchor( backgroundFrameName, 'bottomright', contentFrameName, 'bottomright', 
                         KwestorTracker.TRACKER_BACKGROUND_BORDER, KwestorTracker.TRACKER_BACKGROUND_BORDER );
	end

	-- --- draw the tracker quests
	for idx,questID in ipairs(KwestorTracker.orderedQuests) do
		width,height = KwestorTracker.DrawQuest( questID, reAnchor );
		maxWidth = math.max( maxWidth, width );
		totalHeight = totalHeight + height;
		slot = idx;
	end

	-- --- rezise the tracker content frame
	WindowSetDimensions( contentFrameName, maxWidth, totalHeight );

	-- --- draw the background
	if ( Kwestor.SettingGet('questTrackerBackgroundMode') == 'TRACKER' ) then
		if ( slot > 0 ) then
			KwestorUtility.WindowSetTintColor( backgroundFrameName, Kwestor.SettingGet('questTrackerBackgroundColor') );
			WindowSetAlpha( backgroundFrameName, Kwestor.SettingGet('questTrackerBackgroundAlpha')/100 );
			WindowSetShowing( backgroundFrameName, true );
		else
			WindowSetShowing( backgroundFrameName, false );
		end
	else
		WindowSetShowing( backgroundFrameName, false );
	end

	-- --- adjust the scale
	KwestorTracker.WindowAdjustScale( contentFrameName );

	-- --- cleanup unused quests
	slot = slot + 1;
	frameName = "UI_KwestorTrackerContentQuest"..slot;
	while ( DoesWindowExist(frameName) and WindowGetShowing(frameName) ) do
		WindowSetShowing( frameName, false );
		WindowSetShowing( frameName..'Button', false );
		DestroyWindow( frameName );
		DestroyWindow( frameName..'Button' );
		slot = slot + 1;
		frameName = "UI_KwestorTrackerContentQuest"..slot;
	end
	
end

-- =====================================================================================================
-- Sort the quests
-- =====================================================================================================

function KwestorTracker.CompareTuple( key1, key2, iteration )

	local criteria;

	if ( iteration > 3 ) then
		return( false );
	end

	if ( iteration == 0 ) then
		criteria = 'LOCKED';
	else
		criteria = Kwestor.SettingGet( 'questSortCriteria'..iteration );
	end

	-- ----- no criteria ------------------
	if ( criteria == 'NONE' ) then
		tmp1 = KwestorTracker.trackedQuests[key1].kwestorWARIndex;
		tmp2 = KwestorTracker.trackedQuests[key2].kwestorWARIndex;

		if ( tmp1 < tmp2 ) then
			return( true );
		elseif ( tmp1 > tmp2 ) then
			return( false );
		else
			return( KwestorTracker.CompareTuple(key1,key2,iteration+1) );
		end
	-- ----- locked -----------------------
	elseif ( criteria == 'LOCKED' ) then
		tmp1 = KwestorTracker.trackedQuests[key1].tracking;
		tmp2 = KwestorTracker.trackedQuests[key2].tracking;

		if ( tmp1 and (not tmp2) ) then
			return( true );
		elseif ( (not tmp1) and tmp2 ) then
			return( false );
		else
			return( KwestorTracker.CompareTuple(key1,key2,iteration+1) );
		end
	-- ----- zone -------------------------
	elseif ( criteria == 'ZONE' ) then
		tmp1 = KwestorTracker.IsQuestInPlayerZone( key1 );
		tmp2 = KwestorTracker.IsQuestInPlayerZone( key2 );

		if ( tmp1 and (not tmp2) ) then
			return( true );
		elseif ( (not tmp1) and tmp2 ) then
			return( false );
		else
			return( KwestorTracker.CompareTuple(key1,key2,iteration+1) );
		end
	-- ----- status -----------------------
	elseif ( criteria == 'STATUS' ) then
		tmp1 = KwestorTracker.IsQuestComplete( key1 );
		tmp2 = KwestorTracker.IsQuestComplete( key2 );

		if ( tmp1 and (not tmp2) ) then
			return( false );
		elseif ( (not tmp1) and tmp2 ) then
			return( true );
		else
			return( KwestorTracker.CompareTuple(key1,key2,iteration+1) );
		end
	-- ----- status -----------------------
	elseif ( criteria == 'TITLE' ) then
		tmp1 = KwestorTracker.trackedQuests[key1].name;
		tmp2 = KwestorTracker.trackedQuests[key2].name;

		if ( tmp1 < tmp2 ) then
			return( true );
		elseif ( tmp1 > tmp2 ) then
			return( false );
		else
			return( KwestorTracker.CompareTuple(key1,key2,iteration+1) );
		end
	-- ----- Something else ---------------
	else
		return( KwestorTracker.CompareTuple(key1,key2,iteration+1) );
	end

end

function KwestorTracker.SortQuestCompare( idx1, idx2 )

	return( KwestorTracker.CompareTuple(idx1,idx2,0) );

end

function KwestorTracker.SortQuests()

	table.sort( KwestorTracker.orderedQuests, KwestorTracker.SortQuestCompare );

	for idx,questID in ipairs(KwestorTracker.orderedQuests) do
		KwestorTracker.trackedQuests[questID].kwestorTrackerSlot = idx;
	end

end

function KwestorTracker.SortAndDrawQuests()

	KwestorTracker.SortQuests();
	KwestorTracker.DrawTracker( false );

end

-- =====================================================================================================
-- Wipe all quests
-- =====================================================================================================

function KwestorTracker.WipeTracker()

	local slot = 0;

	KwestorTracker.trackedQuests = {};
	KwestorTracker.orderedQuests = {};
	KwestorTracker.timedQuests = {};

	slot = slot + 1;
	frameName = "UI_KwestorTrackerContentQuest"..slot;
	while ( DoesWindowExist(frameName) and WindowGetShowing(frameName) ) do
		WindowSetShowing( frameName, false );
		WindowSetShowing( frameName..'Button', false );
		slot = slot + 1;
		frameName = "UI_KwestorTrackerContentQuest"..slot;
	end

end

-- =====================================================================================================
-- Alignment was updated
-- =====================================================================================================

function KwestorTracker.UpdateAlignment()

	KwestorTracker.DrawTracker( true );

end

-- =====================================================================================================
-- Anchor data
-- =====================================================================================================

function KwestorTracker.SetAnchor( frame, point, relTo, relPoint, x, y )

	WindowClearAnchors( frame );

	if ( Kwestor.SettingGet('questTrackerAlignment') == 'LEFT' ) then
		WindowAddAnchor( frame, point, relTo, relPoint, x, y );
	else
		point = point:upper();
		point = point:gsub( 'LEFT', 'right' );
		point = point:gsub( 'RIGHT', 'left' );
		point = point:upper();

		relPoint = relPoint:upper();
		relPoint = relPoint:gsub( 'LEFT', 'right' );
		relPoint = relPoint:gsub( 'RIGHT', 'left' );
		relPoint = relPoint:upper();

		x = x * -1;

		WindowAddAnchor( frame, point, relTo, relPoint, x, y );
	end

end


-- =====================================================================================================
-- Adjust window scale
-- =====================================================================================================

function KwestorTracker.WindowAdjustScale( frameName )

	WindowSetScale( frameName, WindowGetScale('UI_KwestorTracker') );

end

-- =====================================================================================================
-- is the quest complete
-- =====================================================================================================

function KwestorTracker.IsQuestComplete( questID )

	return( KwestorTracker.trackedQuests[questID].kwestorIsQuestComplete );

end

-- =====================================================================================================
-- Is player in the same zone as the quest
-- =====================================================================================================

function KwestorTracker.IsQuestInPlayerZone( value )

	if ( type(value) == 'number' ) then													-- number passed: questID
		return( KwestorTracker.trackedQuests[value].kwestorIsQuestInPlayerZone );
	else
		for k,v in pairs(value) do														-- table passed: questZoneData
			if ( v.id == GameData.Player.zone ) then
				return( true );
			end
		end
	end

end

-- =====================================================================================================
-- Is player in RvR zone and quest is RvR
-- =====================================================================================================

function KwestorTracker.QuestFilterRvR( questDataTypes )

	local areaIsRvR = KwestorTracker.playerInRvRZoneArea;																	-- updated in EventQuestListUpdated

	-- ----- RvR area
	if ( areaIsRvR ) then
		return( questDataTypes[GameData.QuestTypes.PLAYER_KILL] or questDataTypes[GameData.QuestTypes.RVR] );
	-- ----- non RvR area
	else
		if ( questDataTypes[GameData.QuestTypes.TRAVEL] ) then																-- even if flagged rvr (e.g. "go to warcamp, meet blah")
			return( true );
		elseif ( questDataTypes[GameData.QuestTypes.PLAYER_KILL] or questDataTypes[GameData.QuestTypes.RVR] ) then
			return( false );
		else
			return( true );
		end
	end

	return( true );

end

-- =====================================================================================================
-- Left Button Clicked
-- =====================================================================================================

function KwestorTracker.LeftButtonClick( flags, x, y )

	local questData = nil;
	local questID = WindowGetId( SystemData.ActiveWindow.name );

	-- shift clicked, link the quest
	if ( RMetUtility.IsShiftPressed(flags) ) then
		questData = DataUtils.GetQuestData( questID );
		EA_ChatWindow.InsertQuestLink( questData );
    -- If we are clicking the same quest button, hide the Tome
    elseif ( WindowGetShowing("TomeWindow") == false  ) then
        MenuBarWindow.ToggleTomeWindow();
    	TomeWindow.OpenToQuest( questID )
    elseif ( TomeWindow.IsShowingQuest(questID) == true ) then
        MenuBarWindow.ToggleTomeWindow();
	else
    	TomeWindow.OpenToQuest( questID )
    end

end

-- =====================================================================================================
-- Mouse Over
-- =====================================================================================================

function KwestorTracker.MouseOverQuest()

	local text, anchor;
	local questID = WindowGetId( SystemData.ActiveWindow.name );
	local frameName = "UI_KwestorTrackerContentQuest"..(KwestorTracker.trackedQuests[questID].kwestorTrackerSlot);

	KwestorTracker.mouseOverQuestID = questID;

    Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil );

    -- Quest Name
    text = KwestorTracker.trackedQuests[questID].name;
    Tooltips.SetTooltipText( 1, 1, text );
    Tooltips.SetTooltipColorDef( 1, 1, Tooltips.COLOR_HEADING );

    -- Quest Text
    text = KwestorTracker.trackedQuests[questID].journalDesc;
    Tooltips.SetTooltipText( 2, 1, text );

    -- Open Tome Text
--    Tooltips.SetTooltipActionText( GetString( StringTables.Default.TEXT_OPEN_TO_QUEST_ENTRY ) );

    Tooltips.Finalize();

    anchor = { Point="bottomleft", RelativeTo=frameName, RelativePoint="topleft", XOffset=0, YOffset=0 };
    Tooltips.AnchorTooltip( anchor );

	-- Highlight the quest
	KwestorTracker.DrawQuest( KwestorTracker.mouseOverQuestID );

end

function KwestorTracker.MouseOverQuestEnd()

	local questID = KwestorTracker.mouseOverQuestID;						-- need to store because I have to set mouseOverQuestID to -1 before calling DrawQuest()

	KwestorTracker.mouseOverQuestID = -1;

	if ( questID >= 0 )  then
		KwestorTracker.DrawQuest( questID );
	end

end

--[[
function EA_Window_QuestTracker.OnMouseOverQuestType()
    local id = WindowGetId( WindowGetParent( SystemData.ActiveWindow.name ) )
    local questId = EA_Window_QuestTracker.currentData[id].questId
    local questData = DataUtils.GetQuestData( questId )
    QuestUtils.CreateQuestTypeTooltip( questData, SystemData.MouseOverWindow.name )
end
]]

function KwestorTracker.DebugTimerInit()

	if ( KwestorTracker.timerEnd == nil ) then
		KwestorTracker.timerEnd = HealGrid.Ticker() + KwestorTracker.DebugTimerMax(); 
	end

end

function KwestorTracker.DebugTimerMax()

	return( 120 );

end

function KwestorTracker.DebugTimerCurr()

	local t = math.floor( KwestorTracker.timerEnd - HealGrid.Ticker() );

	if ( t < 0 ) then
		t = 0;
	end

	return( t );

end

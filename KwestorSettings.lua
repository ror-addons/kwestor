KwestorSettings = {};

KwestorSettingsDefault = {};

KwestorSettingsDefault.showTracker = true;										-- changeable via Nub

KwestorSettingsDefault.questTrackerMaxQuests = 10;
KwestorSettingsDefault.questTrackerAlignment = 'RIGHT';							-- LEFT|RIGHT
KwestorSettingsDefault.questTrackerGrowth = 'DOWN';								-- DOWN|UP
KwestorSettingsDefault.questCompletedIcon = 'QUESTTYPE';						-- QUESTTYPE|COMPLETED
KwestorSettingsDefault.questTrackerBackgroundMode = 'NONE';						-- TRACKER|FRAME|NONE
KwestorSettingsDefault.questTrackerBackgroundColor = 'BLACK';
KwestorSettingsDefault.questTrackerBackgroundAlpha = 30;	
KwestorSettingsDefault.questTrackerAutofill = false;							-- autofill the tracker up to maxQuests

KwestorSettingsDefault.questHighlightNormalAlpha = 70;
KwestorSettingsDefault.questHighlightMouseoverAlpha = 100;

KwestorSettingsDefault.questTitleFont = 'font_clear_medium_bold';
KwestorSettingsDefault.questConditionFont = 'font_clear_small_bold';

KwestorSettingsDefault.questTitleColorCompleted = 'LIME';
KwestorSettingsDefault.questTitleColorTracked = 'YELLOW';
KwestorSettingsDefault.questTitleColorCompletedOtherZone = 'GREEN';
KwestorSettingsDefault.questTitleColorTrackedOtherZone = 'ORANGE';
KwestorSettingsDefault.questConditionColorCompleted = 'GREEN';
KwestorSettingsDefault.questConditionColorTracked = 'WHITE';

KwestorSettingsDefault.questFilterZone = false;
KwestorSettingsDefault.questFilterRvR = false;
KwestorSettingsDefault.questFilterStatus = false;

KwestorSettingsDefault.questConditionFilterCompletedWithCounter = false;
KwestorSettingsDefault.questConditionFilterCompletedSingleObjective = false;
KwestorSettingsDefault.questConditionFilterOtherZone = false;

KwestorSettingsDefault.questSortCriteria1 = 'ZONE';								-- NONE|ZONE|STATUS|TITLE
KwestorSettingsDefault.questSortCriteria2 = 'STATUS';
KwestorSettingsDefault.questSortCriteria3 = 'TITLE';
